﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Waaghals.Robin.Application.Dispatcher;

namespace Waaghals.Robin.Application.Publishers
{
    public class AggregatesPublisherFactory
    {
        private readonly IAggregateDispatcher dispatcher;
        private readonly IServiceScopeFactory serviceScopeFactory;

        public AggregatesPublisherFactory(IAggregateDispatcher dispatcher, IServiceScopeFactory serviceScopeFactory)
        {
            this.dispatcher = dispatcher;
            this.serviceScopeFactory = serviceScopeFactory;
        }

        public AggregatesPublisher Create(TimeSpan duration)
        {
            return new AggregatesPublisher(duration, dispatcher, serviceScopeFactory);
        }
    }
}
