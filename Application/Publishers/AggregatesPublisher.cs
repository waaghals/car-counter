﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Dispatcher;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Common.Extensions;

namespace Waaghals.Robin.Application.Publishers
{
    public class AggregatesPublisher
    {
        private readonly TimeSpan duration;
        private readonly IAggregateDispatcher dispatcher;
        private readonly IServiceScopeFactory serviceScopeFactory;

        public AggregatesPublisher(TimeSpan duration, IAggregateDispatcher dispatcher, IServiceScopeFactory serviceScopeFactory)
        {
            this.duration = duration;
            this.dispatcher = dispatcher;
            this.serviceScopeFactory = serviceScopeFactory;
        }

        public async Task PublishAggregatesInfinitly(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                using (var scope = serviceScopeFactory.CreateScope())
                {
                    var repositoryFactory = scope.ServiceProvider.GetRequiredService<IRepositoryFactory>();
                    var carRepository = repositoryFactory.Create<ICarRepository>();
                    var manufacturerRepository = repositoryFactory.Create<IManufacturerRepository>();
                    var to = DateTimeOffset.Now.Floor(duration);
                    var from = to.Subtract(duration);

                    await DispatchManufacturersAggregates(from, carRepository, manufacturerRepository).ConfigureAwait(false);
                }
                await NextIntervalAsync(duration, cancellationToken).ConfigureAwait(false);
            }
        }

        private async Task DispatchManufacturersAggregates(DateTimeOffset from, ICarRepository carRepository, IManufacturerRepository manufacturerRepository)
        {
            foreach (var manufacturer in manufacturerRepository.GetAll())
            {
                var cars = carRepository.GetCarsRecenltyBuild(from, from.Add(duration), manufacturer);
                var numberOfCars = cars.Count();

                await dispatcher.Dispatch(manufacturer, from, numberOfCars).ConfigureAwait(false);
            }
        }

        private static Task NextIntervalAsync(TimeSpan interval, CancellationToken cancellationToken)
        {
            var now = DateTimeOffset.Now;
            var nextExecutionTime = now.Add(interval);
            var delta = nextExecutionTime - now;

            return Task.Delay(delta, cancellationToken);
        }
    }
}
