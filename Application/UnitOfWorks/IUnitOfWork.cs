﻿using System.Threading.Tasks;
using Waaghals.Robin.Application.Repository;

namespace Waaghals.Robin.Application.UnitOfWorks
{
    public interface IUnitOfWork : IRepositoryFactory
    {
        Task CommitAsync();
    }
}
