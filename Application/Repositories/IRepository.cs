﻿using System.Threading.Tasks;

namespace Waaghals.Robin.Application.Repository
{
    public interface IRepository
    {
        Task CommitAsync();
    }
}
  