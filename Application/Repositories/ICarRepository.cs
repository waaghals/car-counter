﻿using System;
using System.Collections.Generic;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Application.Repository
{
    interface ICarRepository : IRepository
    {
        IEnumerable<Car> GetCarsRecenltyBuild(DateTimeOffset from, DateTimeOffset to, Manufacturer manufacturer);

        void Save(Car car);
    }
}
