﻿namespace Waaghals.Robin.Application.Repository
{
    public interface IRepositoryFactory
    {
        TRepo Create<TRepo>() where TRepo : IRepository;
    }
}
