﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Application.Repository
{
    interface IManufacturerRepository : IRepository
    {
        IEnumerable<Manufacturer> GetAll();

        Task<Manufacturer> GetByNameAsync(string name);
    }
}
