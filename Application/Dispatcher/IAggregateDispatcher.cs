﻿using System;
using System.Threading.Tasks;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Application.Dispatcher
{
    public interface IAggregateDispatcher
    {
        Task Dispatch(Manufacturer manufacturer, DateTimeOffset start, int count);
    }
}
