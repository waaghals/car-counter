﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Waaghals.Robin.Domain.Hubs;

namespace Waaghals.Robin.Infrastructure.Hubs
{
    public sealed class MetricHub : Hub<IMetricHub>
    {
        public Task Subscribe(string manufacturer)
        {
            if (string.IsNullOrWhiteSpace(manufacturer))
            {
                throw new System.ArgumentException("Manufacturer cannot be empty", nameof(manufacturer));
            }

            return Groups.AddToGroupAsync(Context.ConnectionId, manufacturer);
        }

        public Task Unsubscribe(string manufacturer)
        {
            if (string.IsNullOrWhiteSpace(manufacturer))
            {
                throw new System.ArgumentException("Manufacturer cannot be empty", nameof(manufacturer));
            }

            return Groups.RemoveFromGroupAsync(Context.ConnectionId, manufacturer);
        }
    }
}
