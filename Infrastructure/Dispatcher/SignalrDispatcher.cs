﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Dispatcher;
using Waaghals.Robin.Domain.Hubs;
using Waaghals.Robin.Domain.Models;
using Waaghals.Robin.Infrastructure.Hubs;

namespace Waaghals.Robin.Infrastructure.Dispatcher
{
    public class SignalrDispatcher : IAggregateDispatcher
    {
        private readonly IHubContext<MetricHub, IMetricHub> metricHub;

        public SignalrDispatcher(IHubContext<MetricHub, IMetricHub> metricHub)
        {
            this.metricHub = metricHub;
        }

        public Task Dispatch(Manufacturer manufacturer, DateTimeOffset start, int count)
        {
            return metricHub
                    .Clients
                    .Group(manufacturer.Name)
                    .NewAggregate(manufacturer.Name, start, count);
        }
    }
}
