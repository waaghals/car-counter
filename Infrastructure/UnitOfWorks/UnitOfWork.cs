﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Application.UnitOfWorks;
using Waaghals.Robin.Infrastructure.Exceptions;
using Waaghals.Robin.Infrastructure.Repositories;

namespace Waaghals.Robin.Infrastructure.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RobinContext context;
        private readonly IDictionary<Type, IRepository> repositories;

        public UnitOfWork(RobinContext context)
        {
            this.context = context;
            this.repositories = new Dictionary<Type, IRepository>();
        }
        public Task CommitAsync()
        {
            return Task.WhenAll(repositories.Select(r => r.Value.CommitAsync()));
        }

        public TRepo Create<TRepo>() where TRepo : IRepository
        {
            var repoType = typeof(TRepo);

            if (repositories.TryGetValue(repoType, out IRepository repository))
            {
                return (TRepo)repository;
            }

            //TODO Using seperate RepoFactories which where injected using DI would be nicer
            if (repoType == typeof(ICarRepository))
            {
                repositories.Add(repoType, new CarRepository(context));
                return (TRepo)repositories[repoType];
            }

            if (repoType == typeof(IManufacturerRepository))
            {
                repositories.Add(repoType, new ManufacturerRepository(context));
                return (TRepo)repositories[repoType];
            }

            throw new RepositoryNotFoundException();
        }
    }
}
