﻿using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Publishers;

namespace Waaghals.Robin.Infrastructure.Hosting
{
    public class HostedAggregatePublisher : IHostedService
    {
        private readonly AggregatesPublisherFactory publisherFactory;

        public HostedAggregatePublisher(AggregatesPublisherFactory publisherFactory)
        {
            this.publisherFactory = publisherFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var publisher = publisherFactory.Create(TimeSpan.FromSeconds(10));
            return publisher.PublishAggregatesInfinitly(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
