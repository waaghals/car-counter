﻿using System;
using Microsoft.EntityFrameworkCore;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Infrastructure
{
    public class RobinContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }

        public RobinContext(DbContextOptions<RobinContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Manufacturer>(manufacturer =>
            {
                manufacturer.HasKey(m => m.Id);
                manufacturer.Property(m => m.Id).ValueGeneratedNever();
                manufacturer.Property(m => m.Name).IsRequired();
                manufacturer.HasIndex(m => m.Name).IsUnique();
                manufacturer.Property(m => m.Secret).IsRequired();
            });

            modelBuilder.Entity<Car>(car =>
            {
                car.HasKey(c => c.Id);
                car.Property(c => c.Id).ValueGeneratedNever();
                car.Property(c => c.Color).IsRequired();
                car.Property(c => c.CreatedAt).IsRequired();
                car.HasIndex(c => c.CreatedAt);
                car.HasOne(c => c.Manufacturer).WithMany(m => m.Cars).IsRequired();
            });
        }
    }
}
