﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Infrastructure.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly RobinContext context;

        public CarRepository(RobinContext context)
        {
            this.context = context;
        }

        public Task CommitAsync()
        {
            return context.SaveChangesAsync();
        }

        public IEnumerable<Car> GetCarsRecenltyBuild(DateTimeOffset from, DateTimeOffset to, Manufacturer manufacturer)
        {
            if (manufacturer == null)
            {
                throw new ArgumentNullException(nameof(manufacturer));
            }

            return context.Cars.Where(c => c.CreatedAt > from && c.CreatedAt <= to && c.Manufacturer == manufacturer);
        }

        public void Save(Car car)
        {
            context.Cars.Add(car);
        }
    }
}
