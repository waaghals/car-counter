﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Domain.Exceptions;
using Waaghals.Robin.Domain.Models;

namespace Waaghals.Robin.Infrastructure.Repositories
{
    public class ManufacturerRepository : IManufacturerRepository
    {
        private readonly RobinContext context;

        public ManufacturerRepository(RobinContext context)
        {
            this.context = context;
        }

        public Task CommitAsync()
        {
            //TODO could do with a readonly repository interface, because currenly no need to create manufacturer
            return Task.CompletedTask;
        }

        public IEnumerable<Manufacturer> GetAll()
        {
            return context.Manufacturers;
        }

        public async Task<Manufacturer> GetByNameAsync(string name)
        {
            if (name == null)
            {
                throw new System.ArgumentNullException(nameof(name));
            }

            var manufacturer = await context.Manufacturers.SingleOrDefaultAsync(m => m.Name == name).ConfigureAwait(false);

            if (manufacturer == null)
            {
                throw new ManufacturerNotFoundException($"Manufacturer with name {name} not found");
            }

            return manufacturer;
        }
    }
}
