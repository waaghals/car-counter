﻿using System.ComponentModel.DataAnnotations;

namespace Waaghals.Robin.Infrastructure.Models
{
    public class Car
    {
        [Required]
        [RegularExpression("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", ErrorMessage = "Color should be valid Hex color")]
        public string Color { get; set; }
    }
}
