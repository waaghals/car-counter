﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Application.UnitOfWorks;
using Waaghals.Robin.Domain.Exceptions;
using Waaghals.Robin.Domain.Models;
using CarPostModel = Waaghals.Robin.Infrastructure.Models.Car;

namespace Waaghals.Robin.Infrastructure.Controllers
{
    public class ManufacturerController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ManufacturerController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> Webhook(string manufacturerName, [FromBody] CarPostModel model, [FromHeader] string secret)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var manufacturerRepository = unitOfWork.Create<IManufacturerRepository>();

            try
            {
                var manufacturer = await manufacturerRepository.GetByNameAsync(manufacturerName);
                //Having the client sign the whole request and validating it here would be neater
                if (manufacturer.Secret != secret)
                {
                    return Unauthorized();
                }

                var car = new Car(manufacturer, model.Color);

                var carRepository = unitOfWork.Create<ICarRepository>();
                carRepository.Save(car);

                await unitOfWork.CommitAsync();

                return Accepted();
            }
            catch (ManufacturerNotFoundException)
            {
                return NotFound(manufacturerName);
            }

        }
    }
}
