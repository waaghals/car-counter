﻿using System;

namespace Waaghals.Robin.Domain.Models
{
    public class Car
    {
        public Guid Id { get; }
        public virtual Manufacturer Manufacturer { get; internal set; }
        public DateTimeOffset CreatedAt { get; }
        public string Color { get; set; }

        private Car(Guid id, DateTimeOffset createdAt, string color)
        {
            Id = id;
            createdAt = CreatedAt;
            color = Color;
        }

        public Car(Manufacturer manufacturer, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                throw new ArgumentException("Color cannot be empty", nameof(color));
            }

            Id = Guid.NewGuid();
            CreatedAt = DateTimeOffset.Now;
            Manufacturer = manufacturer ?? throw new ArgumentNullException(nameof(manufacturer));
            Color = color;
        }
    }
}
