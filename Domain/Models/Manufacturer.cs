﻿using System;
using System.Collections.Generic;

namespace Waaghals.Robin.Domain.Models
{
    public class Manufacturer
    {
        public Guid Id { get; internal set; }
        public string Name { get; set; }
        public string Secret { get; set; }
        public virtual ICollection<Car> Cars { get; internal set; }

        private Manufacturer(Guid id, string name, string secret)
        {
            Id = id;
            Name = name;
            Secret = secret;
        }

        public Manufacturer(string name, string secret)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Name cannot be empty", nameof(name));
            }

            if (string.IsNullOrWhiteSpace(secret))
            {
                throw new ArgumentException("Secret cannot be empty", nameof(secret));
            }

            Id = Guid.NewGuid();
            Name = name;
            Secret = secret;
            Cars = new List<Car>();
        }
    }
}
