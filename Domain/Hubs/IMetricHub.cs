﻿using System;
using System.Threading.Tasks;

namespace Waaghals.Robin.Domain.Hubs
{
    public interface IMetricHub
    {
        Task NewAggregate(string serie, DateTimeOffset start, int count);
    }
}
