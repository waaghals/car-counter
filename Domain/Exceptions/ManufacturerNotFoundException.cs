﻿namespace Waaghals.Robin.Domain.Exceptions
{
    public class ManufacturerNotFoundException : System.Exception
    {
        public ManufacturerNotFoundException(string message) : base(message)
        {

        }
    }
}
