﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Waaghals.Robin.Application.Dispatcher;
using Waaghals.Robin.Application.Publishers;
using Waaghals.Robin.Application.Repository;
using Waaghals.Robin.Application.UnitOfWorks;
using Waaghals.Robin.Infrastructure;
using Waaghals.Robin.Infrastructure.Dispatcher;
using Waaghals.Robin.Infrastructure.Hosting;
using Waaghals.Robin.Infrastructure.Hubs;
using Waaghals.Robin.Infrastructure.Repositories;
using Waaghals.Robin.Infrastructure.UnitOfWorks;

namespace Waaghals.Robin
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSignalR();
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<RobinContext>(options => options.UseSqlServer("Data Source=DESKTOP-6HU55VV\\SQLEXPRESS;Initial Catalog=robin;Integrated Security=True;"));

            services.AddCors();
            services.AddScoped<ICarRepository, CarRepository>();
            services.AddScoped<IManufacturerRepository, ManufacturerRepository>();
            services.AddSingleton<IHostedService, HostedAggregatePublisher>();
            services.AddSingleton<AggregatesPublisherFactory>();
            services.AddSingleton<IAggregateDispatcher, SignalrDispatcher>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepositoryFactory, UnitOfWork>();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "webhook",
                    template: "/webhook/{*manufacturerName}",
                    defaults: new
                    {
                        controller = "Manufacturer",
                        action = "Webhook",
                    });
            });

            app.UseCors(policy => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseSignalR(routes =>
            {
                routes.MapHub<MetricHub>("/metrics");
            });

            app.UseStaticFiles();

            //    app.Run(async (context) =>
            //    {
            //        await context.Response.WriteAsync("Nope");
            //        return;
            //        var unitOfWork = context.RequestServices.GetRequiredService<IUnitOfWork>();
            //        var manufacturerRepository = unitOfWork.Create<IManufacturerRepository>();
            //        var manufacturer = await manufacturerRepository.GetByNameAsync("volvo");
            //
            //        var car = new Car(manufacturer, "green");
            //
            //        var carRepository = unitOfWork.Create<ICarRepository>();
            //        carRepository.Save(car);
            //
            //        await unitOfWork.CommitAsync();
            //
            //        context.Response.StatusCode = 201;
            //
            //        await context.Response.WriteAsync($"Created car at {car.CreatedAt}");
            //    });
        }
    }
}
